
class Breathing {   

  // Location and size
  float x, y;
  float rad;
  // Angle for oscillation
  float theta;  
  float dtheta;

  Breathing() { 
    // Initialize variables randomly
    x = 250;
    y = 250;  
    rad = random(2,100);
    theta = random(PI);  
    dtheta = random(0.01, 0.1);
    
  }   

  // Increment theta
  void breath() {
    theta += dtheta;
  }   

  void display() { 
    // Map size based on sine function  
    float r = rad + rad * (sin(theta) + 1);
    // Display circle
    fill(112,22,144);
    stroke(1);
    ellipse(x, y, r, r);
  }
}   